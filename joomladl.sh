#!/bin/sh

# DOWNLOAD PATH VARIABLE
destinationdir=~/bin/assets

# COLOR VARIABLES
# ${cyan} or ${lcyan} are starting tag for color
# ${nc} = is the closing color tag
# usage = echo -e "The word ${cyan}cyan${nc} will be colored cyan."
cyan='\033[0;36m'
lcyan='\033[1;36m'
nc='\033[0m'

# CLEAR SCREEN
clear

# INTRODUCTION
printf "
${lcyan}Introduction:${nc}
This is a tutorial script used to download the latest Stable Full Joomla installer package.
Unlike other Content Management Systems, Joomla does not have a unique URL that ensures it's users are downloading the latest version of the Stable Full installer package.

To obtain the latest version of the Stable Full Joomla installer package, we need to manually visit joomla.org in our browser and navigate to the downloads section to select and download the latest Stable Full Joomla installer package with the desired extension required.

Real Linux users of course want to do all this programatically. 

Fortunately for us, one of Joomla's core xml files contains a download url for system updates.
We will be taking advantage of the fact that it always contains the latest Joomla version number.

And so this script will programatically download this core xml file with wget.
It will then parse it with xmlstarlet to extract the download url.
It will use cut and tr for string manipulation and substitution.

Thus we will programatically deconstruct this download url and reassemble it to form a new download url that hopefully ensures we are always downloading the latest Stable Full Joomla installer package.
"

# WGET
printf "
${lcyan}The wget command:${nc}
The ${cyan}wget${nc} command can be used to download files.
We will be using wget here to download a core Joomla xml file.
Note the use of the ${cyan}-P${nc} option.
It enables us to specify the download path for our file.

Here's the wget command we need to download the xml file:
${cyan}wget https://update.joomla.org/core/sts/extension_sts.xml -P $destinationdir${nc}

"

# XML FILE EXISTS TEST
file=$destinationdir/extension_sts.xml
if 
[ -f "$file" ];
then
echo "
Status: The xml file has been downloaded.
"
else
wget https://update.joomla.org/core/sts/extension_sts.xml -P $destinationdir && echo "Status: The xml file has succesfully downloaded.
"
fi


# USE XMLSTARLET
printf "
${lcyan}The xmlstarlet command:${nc}
We just used wget to download the file ${cyan}extension_sts.xml${nc}
This xml file contains multiple download urls for specific Joomla packages.
What we need to do here is extract the download url for the latest Joomla Stable-Update_Package.
We will use the ${cyan}xmlstarlet${nc} utility to extract the exact url we need.

Here's the xmlstarlet command that targets the specific url we want and assigns it to ${cyan}\$updatepkgurl${nc}:
${cyan}updatepkgurl=xmlstarlet sel -t -v '//updates/update[3]/downloads/downloadurl' $destinationdir/extension_sts.xml${nc}
"
updatepkgurl=$(xmlstarlet sel -t -v '//updates/update[3]/downloads/downloadurl' $destinationdir/extension_sts.xml)

printf "
And xmlstarlet returns the Stable Update Package download url ${cyan}\$updatepkgurl${nc}:
${cyan}$updatepkgurl${nc}

Let's now go to joomla.org
Here we can see that the url to the Stable Full download package is different:
${cyan}https://downloads.joomla.org/cms/joomla4/4-0-3/Joomla_4-0-3-Stable-Full_Package.zip${nc}

Note that whilst the path and version to both these files is near identical, what really differs is simply the filename.

It's time to start dissasembling the Stable Update download url xmlstarlet has provided us...

"

# DETERMINING JOOMLA VERSION NUMBER WITH CUT
printf "
${lcyan}Extracting the Joomla version number:${nc}
We need to extract the Joomla version number from our Stable Update download url.
To do this we will use the ${cyan}cut${nc} command.
What we will do is take the Stable Update download url and pipe it into cut.

Note how we set the delimiter ${cyan}-d${nc} to ${cyan}'\\\\'${nc} to cut the url up into slugs.
Also note how we set the field value ${cyan}-f${nc} to ${cyan}6${nc} to capture the 6th field or slug.

Here's the exact command we ran:
"
escversioncmd=$(echo 'https://downloads.joomla.org/cms/joomla4/4-0-3/Joomla_4.0.3-Stable-Update_Package.zip | cut -d '\''/'\'' -f 6)')
echo "${cyan}$escversioncmd${nc}"

version=$(echo $updatepkgurl | cut -d '/' -f 6)
printf "
And here's the actual version number the command returned:
$version

We need this version number to create our filename.

"

# DASH TO DOT CONVERSION WITH TR
printf "
${lcyan}A short note on converting characters:${nc}
The version number we just extracted from our Stable Update Download url is $version
Note how this version number is dash separated.

Now examine the Full Update Download url we got from joomla.org earlier.
Note how the version number within it's file name is dot separated.

There may be situations where we need to convert characters within filenames.
The ${cyan}tr${nc} tool enables us to do this rather elegantly.

Let's modify our earlier command by additionaly piping the results into tr to convert that 4-0-3 to 4.0.3

Here's the revised command:
"
escversion2cmd=$(echo 'https://downloads.joomla.org/cms/joomla4/4-0-3/Joomla_4.0.3-Stable-Update_Package.zip | cut -d '\''/'\'' -f 6 | tr - .)')
echo "${cyan}$escversion2cmd${nc}"

version2=$(echo $updatepkgurl | cut -d '/' -f 6 | tr - .)
printf "
And here's the version number this command returns:
$version2

We now have our dot seperated version number.

However our original $version format was the one we needed and so all is good.
We just wanted to take the opportunity to show how useful and elegant the tr command can be!

"

# GENERATE FILENAME
printf "
${lcyan}Generating the Stable Full Package filename:${nc}
The ${cyan}filename${nc} can be assembled by inserting the ${cyan}\$version${nc} variable into the following string:
"
escfilenamecmd=$(echo 'filename=Joomla_$version-Stable-Full_Package.zip')
echo "${cyan}$escfilenamecmd${nc}"

printf "
Here's what our ${cyan}\$filename${nc} variable now returns:
"
filename=$(echo "Joomla_$version-Stable-Full_Package.zip")
echo "${cyan}$filename${nc}

The next thing to work on is the filepath.
"

# GENERATE FILEPATH
printf "
${lcyan}Generating the filepath:${nc}
Using ${cyan}cut${nc}, let's extract the filepath from our ${cyan}\$updatepkgurl${nc} and assign it to the variable ${cyan}\$filepath${nc}.
Note how this time around we have used a range for our field value.

Here's what our ${cyan}cut${nc} command looks like:
"
escfilepathcmd=$(echo 'filepath=$updatepkgurl | cut -d '\''/'\'' -f 1-5)')
echo "${cyan}$escfilepathcmd${nc}"

printf "
And here's the value ${cyan}\$filepath${nc} returns:
"
filepath=$(echo $updatepkgurl | cut -d '/' -f 1-6 )
echo "${cyan}$filepath${nc}
"

# GENERATE DOWNLOAD URL
printf "
${lcyan}Assembling the download url for Stable Full Package:${nc}
It's time now to assemble our download url.
By joining our ${cyan}\$filename${nc} and ${cyan}\$filepath${nc} variables we are able to form our download url.

Heres what the command looks like:
"
escfullpkgurlcmd=$(echo 'fullpkgurl=$filepath/$filename')
echo "${cyan}$escfullpkgurlcmd${nc}"

printf "
And here's the value ${cyan}\$fullpkgurl${nc} returns:
"
fullpkgurl=$(echo "$filepath/$filename")
echo "${cyan}$fullpkgurl${nc}
"

# DOWNLOAD JOOMLA STABLE FULL PACKAGE WITH WGET
printf "
${lcyan}Downloading the latest Stable Full Package zip file with get:${nc}
It's finally time to download the latest full Joomla! installer.
Once again we will use ${cyan}wget${nc} to download it for us.

Heres what our wget command to download Joomla! looks like:
"
escdownloadcmd=$(echo 'wget -O $destinationdir/$filename $fullpkgurl')
echo "${cyan}$escdownloadcmd${nc}"

printf "
The ${cyan}-O${nc} option enables us to define the download path and name of the file.

Let's start the download...
"

# FILE EXISTS TEST
file=$destinationdir/$filename
if 
[ -f "$file" ];
then
echo "
Status: $filename has already been downloaded.
"
else
wget -O $destinationdir/$filename $fullpkgurl && echo "Status: $filename has succesfully downloaded.
"
fi